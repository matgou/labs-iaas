#!/usr/bin/python
# -*- encoding: utf-8 -*-
'''
First, install the latest release of Python wrapper: $ pip install ovh
'''
import json
import ovh
import sys

class OVHCli(object):
    def list_node(self,Arguments):
        result = self.client.get('/kube/' + Arguments[5] + '/publiccloud/node')
        return result

    def delete_node(self, Arguments):
        result = self.client.delete('/kube/' + Arguments[5] + '/publiccloud/node/' + Arguments[6])
        return result 

    def add_node(self, Arguments):
        result = self.client.post('/kube/' + Arguments[5] + '/publiccloud/node', 
		flavorName=Arguments[6],
		name=Arguments[7],
	)
	return result

    def get_kubeconfig(self, Arguments):
        result = self.client.get('/kube/' + Arguments[5] + '/kubeconfig')
        return result
    
    def main(self):
        # Instanciate an OVH Client.
        # You can generate new credentials with full access to your account on
        # the token creation page
        self.client = ovh.Client(
            endpoint='ovh-eu',              # Endpoint of API OVH Europe (List of available endpoints)
            application_key=sys.argv[2],    # Application Key
            application_secret=sys.argv[3], # Application Secret
            consumer_key=sys.argv[4],       # Consumer Key
        )
        
        functions = {
	  'add-node':    self.add_node,
          'list-node':   self.list_node,
          'delete-node': self.delete_node,
          'get-kubeconfig': self.get_kubeconfig
        }
        
        
        # Get the function from switcher dictionary
        func = functions.get(sys.argv[1])
        # Execute the function
        print json.dumps(func(sys.argv), indent=4)

if __name__ == "__main__":
    # execute only if run as a script
    o=OVHCli()
    o.main()
