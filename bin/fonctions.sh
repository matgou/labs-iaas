#!/bin/bash
###################################################################################
#@ fonctions.sh
#@  Fonctions generiques pour gerer le labs
###################################################################################

###################################################################################
#@ Ecrire une ligne dans le logs
ecrire_log() {
  _TS=$( date "+%d/%m/%Y %H:%M:%S" )
  echo "[$_TS] $*" | tee -a $LABS_PATH/traces.log
  unset _TS
}
###################################################################################

###################################################################################
#@ Affiche le sha1 du texte en parametre
get_sha1() {
  _PLAYLOAD=$( echo $* )
  echo $_PLAYLOAD | openssl sha1 -binary | xxd -p
  unset _PLAYLOAD
}
###################################################################################

###################################################################################
#@ Recupere un token si l'actuel a plus d'une journee
get_CK() {
  _PRIVATE_KEY_FIC=$( find $LABS_PATH/private.key -mtime -1 )
  if [ -z $_PRIVATE_KEY_FIC ]
  then
    rm $_PRIVATE_KEY_FIC
    ovh_api_get_CK
  else
    source $_PRIVATE_KEY_FIC
  fi
  unset _PRIVATE_KEY_FIC
}

ovh_api_get_CK() {
  _CREDENTIAL_RESULT=$( curl -XPOST -H"X-Ovh-Application: ${OVH_AK}" -H "Content-type: application/json" \
    https://eu.api.ovh.com/1.0/auth/credential  -d '{
      "accessRules": [{ "method": "GET", "path": "/*" }, { "method": "POST", "path": "/*"}, { "method": "DELETE", "path": "/*"}, { "method": "PUT", "path": "/*"}],
      "redirection":"https://api.ovh.com/console/"
      }' )
  ecrire_log "Status du token : $( echo $_CREDENTIAL_RESULT | $LABS_PATH/jq -r .state )"
  ecrire_log "URL de validation: $( echo $_CREDENTIAL_RESULT | $LABS_PATH/jq -r .validationUrl )"

  OVH_CK=$( echo $_CREDENTIAL_RESULT | $LABS_PATH/jq -r .consumerKey )
  ecrire_log "OVH_CK=${OVH_CK}"

  echo "export OVH_CK=${OVH_CK}" > $LABS_PATH/private.key

  ecrire_log "appuyer sur une touche après avoir validé le token"
  read
  unset _CREDENTIAL_RESULT
}
###################################################################################

###################################################################################
#@ Utilise l'api OVH pour affiche la liste des nodes
ovh_api() {
  _Action=$1
  shift
  #ecrire_log python $LABS_PATH/ovh-cli.py $_Action $OVH_AK $OVH_AS $OVH_CK $K8_UUID $*
  python $LABS_PATH/ovh-cli.py $_Action $OVH_AK $OVH_AS $OVH_CK $K8_UUID $*
  RC=$?
  unset _Action
  return $RC
}
###################################################################################

###################################################################################
#@ Utilise l'api OVH pour affiche la liste des nodes
kubectl_api() {
  if [ ! -f $LABS_PATH/.kube-config ]
  then
    ovh_api get-kubeconfig | $LABS_PATH/jq -r .content > $LABS_PATH/.kube-config
  fi


  $LABS_PATH/kubectl $*
  RC=$?
  return $RC
}
###################################################################################
