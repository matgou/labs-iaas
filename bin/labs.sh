#!/bin/bash
###################################################################################
#@ Script: labs.sh
#@ Description: Conctrole du labs
#@ Utilisation:
#@   ./labs.sh <action> [param]
###################################################################################

###################################################################################
# Calcul de l'environnement de travail
cd $( dirname $0 )
LABS_PATH=$( pwd )
cd - >/dev/null
###################################################################################

###################################################################################
# Chargement de la configuration
source $LABS_PATH/../config/config.param
###################################################################################

###################################################################################
# Chargement des fonctions
source $LABS_PATH/fonctions.sh
###################################################################################

ecrire_log "labs.sh : Gestion du labs"

###################################################################################
# Verification du token
get_CK
###################################################################################

_ACTION=$1
shift

case "$_ACTION" in
#@    - help : Affiche l'aide
  "help")
    grep -E "^#@" $0 | grep -v grep
    exit 1
  ;;
###################################################################################
#@
#@ K8s - Cluster:
#@    - list-node: Affiche la liste des nodes kubernetes du labs
  "list-node")
    ecrire_log "Affichage des noeuds disponnibles: "
    ecrire_log ""
    ecrire_log "NOM                  ID                                      DATE                         STATUS"
    ovh_api list-node | $LABS_PATH/jq -r '.[] | "\(.name)    \(.id)    \(.createdAt)    \(.status)"' | while read line
    do
	ecrire_log "$line"
    done
    
    exit $?
  ;;
  "add-node")
#@    - add-node: Ajoute un node kubernetes du labs
    NODE_TYPE=b2-7
    NODE_NAME=worker-$( openssl rand -hex 5 )
    ecrire_log "Ajout d'un node type $NODE_TYPE, name=$NODE_NAME"
    ovh_api add-node $NODE_TYPE $NODE_NAME
    exit $?
  ;;
#@    - delete-node: Supprime un node kubernetes du labs
  "delete-node")
     NODE_ID=$1
     ecrire_log "Suppression du node ID=$NODE_ID"
     ovh_api delete-node $NODE_ID
     exit $?
  ;;
#@    - get-kubeconfig: Initialise un fichier de configuration pour utiliser kubectl
#@
  "get-kubeconfig")
     ecrire_log "Recuperation du fichier kube-config de kubectl"
     mkdir -p ~/.kube 2>&1 >/dev/null
     ovh_api get-kubeconfig | $LABS_PATH/jq -r .content > ~/.kube/config
     RC=$?
#     ecrire_log "mettre dans le fichier .profile : KUBECONFIG=$LABS_PATH/.kube-config"
     exit $RC
  ;;
###################################################################################
#@
#@ K8s - Pods:
#@    - list-pods: Affiche les pods de l'infrastructure
  "list-pods")
     ecrire_log "Affichage de la liste des pods du labs"
     ecrire_log ""
     kubectl_api get pods -A | while read line
     do
        ecrire_log "$line"
     done
     exit $RC
  ;;
#@    - delete-pods: Supprime un pods de l'infrastructure
  "delete-pods")
     POD_NAME=$1
     ecrire_log "Suppression du pods name=$POD_NAME"
     kubectl_api delete pods $POD_NAME --grace-period=0 --force | while read line
     do
        ecrire_log "$line"
     done
     RC=$?
     exit $RC
  ;;
#@    - shell: Lance un shell dans le pod
  "shell")
     NAMESPACE=$1
     POD_NAME=$2
     kubectl_api --namespace=$NAMESPACE exec -it $POD_NAME -- /bin/bash
     exit $RC
  ;;
###################################################################################
#@
#@ K8s - Deployments:
#@    - list-deployments: Affiche la liste des déploiements
  "list-deployments")
     ecrire_log "Affichage de la liste des deployments du labs"
     ecrire_log ""

     kubectl_api get deployments -A | while read line
     do
        ecrire_log "$line"
     done
     exit $?
  ;;
#@    - delete-deployments: Supprime un deployments
  "delete-deployments")
     NAMESPACE=$1
     DEP_NAME=$2
     ecrire_log "Suppression du deployment name=$DEP_NAME"
     kubectl_api delete deployments $DEP_NAME --namespace=$NAMESPACE | while read line
     do
        ecrire_log "$line"
     done
     RC=$?
     kubectl_api delete services ${DEP_NAME} --namespace=$NAMESPACE | while read line
     do
        ecrire_log "$line"
     done
     exit $RC
  ;;
#@    - add-deployments: Creation d'un déploiements
  "add-deployments")
     _FIC_DEP=$1
     kubectl_api apply -f $_FIC_DEP
     ;;
#@    - expose-deployments: Ajout d'un déploiements comme service
  "expose-deployments")
     DEP_NAME=$1
     kubectl_api expose deployment $DEP_NAME --type=LoadBalancer --name=${DEP_NAME}
     ;;
###################################################################################
#@
#@ K8s - Services:
#@    - list-services
  "list-services")
    ecrire_log "Affichage de la liste des services réseaux du labs"
    ecrire_log ""

    kubectl_api get services -A | while read line
     do
        ecrire_log "$line"
     done
  ;;
  *)
    ecrire_log "Action '$_ACTION' non connue"
    grep -E "^#@" $0 | grep -v grep
    exit 1
esac
