#!/usr/bin/env python 
''' 
Custom dynamic inventory script for Ansible, in Python. 
Retrive pods list and provision it
''' 

import os 
import sys 
import argparse 

from kubernetes import client, config

try: 
    import json 

except ImportError: 
    import simplejson as json 

class ExampleInventory(object): 
    def __init__(self): 
# Configs can be set in Configuration class directly or using helper utility
        config.load_kube_config()
        self.api = client.CoreV1Api()
        self.read_cli_args() 
        # Called with `--list`. 
        if self.args.list: 
            self.inventory = self.get_inventory() 
        # If no groups or vars are present, return an empty inventory. 
        else: 
            self.inventory = self.empty_inventory() 
        print(json.dumps(self.inventory)) 

    # Empty inventory for testing. 
    def empty_inventory(self): 
        return {'_meta': {'hostvars': {}}} 
 

    # Read the command line args passed to the script. 
    def read_cli_args(self): 
        parser = argparse.ArgumentParser() 
        parser.add_argument('--list', action = 'store_true') 
        parser.add_argument('--host', action = 'store') 
        self.args = parser.parse_args() 

    def get_inventory(self):
        inventory = {
            'docker': {
                'hosts': ['docker-1']
            }, '_meta': {'hostvars': {
                'docker-1': {
                    'ansible_host': 'docker-1.kapable.info',
                    'ansible_user': 'debian'
                }
            }}
        }
        ret = self.api.list_service_for_all_namespaces(watch=False)
        for i in ret.items:
            try:
#                app=i.metadata.labels.app
                l=i.metadata.labels
                namespace=i.metadata.namespace
                fqdn=i.status.load_balancer.ingress[0].hostname
                # print l['app']
                # print l['service']
                # print namespace
                # print fqdn
                if inventory.has_key(namespace) == False:
                    inventory[namespace] = {'hosts':[], 'vars':{}}
                inventory[namespace]['hosts'].append(l['app'])
                inventory['_meta']['hostvars'][l['app']] = {}
                inventory['_meta']['hostvars'][l['app']]['ansible_host'] = fqdn


            except AttributeError as err:
                pass
                # print("AttributeError error: {0}".format(err))
            except TypeError as err:
                pass
                # print("TypeError error: {0}".format(err))
        return inventory
 
    # Example inventory for testing. 
    def example_inventory(self): 
        return { 
            'servergroup': { 
                'hosts': ['nodeA', 'nodeB', 'nodeC'], 
                'vars':{ 
                } 
            }, 
        } 

# Get the inventory. 
ExampleInventory() 
